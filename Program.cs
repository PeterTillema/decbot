﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Data;
using Meebey.SmartIrc4net;
using MySql.Data.MySqlClient;
using Merthsoft.DynamicConfig;
using System.Reflection;

namespace Merthsoft.DecBot3 {
	partial class DecBot {
		private const string PlusPlusRegEx = "\\S+(?=\\+\\+)";

		private static IrcClient irc;

		private static DisposableConnection Connection { get; set; }
		private static MySqlCommand Command { get; set; }

		private static Dictionary<string, Action<string, string, string, string[]>> ChatCommandProcessors;
		private static Dictionary<string, Action<string[]>> ConsoleCommandProcessors;

		private static Random random;

		private static string executingLocation = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory.FullName;
		private static string logLocation = Path.Combine(executingLocation, "log.txt");
		private static string configLocation = Path.Combine(executingLocation, "decbot.ini");

		private static Dictionary<string, Regex> chatBots;
		private static int randomChance;
		private static List<string> randomResponses;
		private static int lineTimeout;
		private static string site;

        private static dynamic config;

        static void Main(string[] args) {
            WriteConsoleMessage("Starting DecBot.", MessageDirection.Notification);
            ResetScope();
            InitializeCommandProcessors();
            config = ReloadConfig();

            Thread.CurrentThread.Name = "Main";
            new Thread(ReadCommands).Start();

            connect();
        }

        private static void connect() {
            WriteConsoleMessage(string.Format("Connecting to: {0} ", config.IRC.server), MessageDirection.Notification);
            irc = new IrcClient() {
                SendDelay = 400,
                ActiveChannelSyncing = true,
                CtcpVersion = config.IRC.realname, 
                AutoReconnect = true,
                AutoRejoin = true,
                AutoRejoinOnKick = true,
                AutoRelogin = true
            };

            irc.OnChannelMessage += OnChannelMessageReceived;
            irc.OnRawMessage += OnRawMessage;
            irc.OnWriteLine += OnWriteLine;
            irc.OnDisconnected += OnDisconnected;

            // the server we want to connect to, could be also a simple string
            string[] serverlist = new string[] { config.IRC.server };
            int port = 6667;

            try {
                irc.Connect(serverlist, port);
            } catch (ConnectionException e) {
                System.Console.WriteLine("Couldn't connect! Reason: " + e.Message);
                //Exit();
            }

            irc.Login(config.IRC.nickname, config.IRC.realname, 0, config.IRC.nickname);

            foreach (string chan in config.IRC.channels.Split(',')) {
                irc.RfcJoin(chan);
            }

            irc.Listen();
        }

        private static dynamic ReloadConfig() {
            var config = Merthsoft.DynamicConfig.Config.ReadIni(configLocation);

			chatBots = new Dictionary<string, Regex>();
			foreach (var bot in config.ChatBots) {
				try {
					chatBots[bot.Key.ToLower()] = new Regex(bot.Value, RegexOptions.IgnoreCase);
				} catch (Exception ex) {
					WriteErrorToConsole("Cannot load chatbot {0}, error in regex {1}: {2}", bot.Key, bot.Value, ex.Message);
					chatBots[bot.Key.ToLower()] = null;
				}
			}

			randomResponses = new List<string>(config.Bot.randomResponses.Split(','));
			randomChance = int.Parse(config.Bot.randomChance);
			lineTimeout = int.Parse(config.Bot.lineTimeout);
			site = config.Bot.site;

            Console.Title = config.IRC.realname;

            string protocol = "socket";
            try {
                protocol = config.Database.protocol;
            } catch {
                WriteErrorToConsole("Protocol not defined, defaulting to socket.");
            }
            string connectionString = string.Format("Server={0};Database={2};Uid={3};Pwd={1};Protocol={4}", config.Database.server, config.Database.password, config.Database.database, config.Database.user, protocol);
            Connection = new DisposableConnection(new MySqlConnection(connectionString));

            return config;
		}

		static void OnDisconnected(object sender, EventArgs e) {
			LogMessage("server", MessageDirection.Notification, "Disconnected from server. Attempting reconnect.");
			WriteConsoleMessage("Disconnected from server. Attempting reconnect.", MessageDirection.Notification);
            while (!irc.IsConnected) {
                connect();
                Thread.Sleep(2000);
            }
		}

		private static void InitializeCommandProcessors() {
			ChatCommandProcessors = new Dictionary<string, Action<string, string, string, string[]>>() {
				// Karma stuff
				{"!karma", GetKarma},
				{"!kamra", GetKarma},
				{"!karm", GetKarma},
				{"!krama", GetKarma},
				{"!top", GetTop},
				{"!link", LinkNames},
				{"!unlink", UnlinkNames},
				{"!links", CheckLinks},
				{"!kerma", GetKerma},
				// API Calls
				{"!calc", Calc},
				{"!clac", Calc},
				{"!weather", Weather},
				{"!waehter", Weather},
				{"!waether", Weather},
				// Various other things
				{"!resetscope", ResetScope},
				{"!conv", BaseConvert},
				{"!base", BaseConvert},
				{"!total", GetTotals},
				//{"!merthese", Merthterpret},
				// Quotes
				{"!q", GetQuote},
				{"!qsay", GetQuote},
				{"!quote", GetQuote},
				{"!qfind", GetQuote},
				{"!qsearch", GetQuote},
				{"!qadd", AddQuote},
				{"!quoteadd", AddQuote},
				{"!addquote", AddQuote},
				{"!qdel", DeleteQuote},
				{"!site", SaySite},
			};

			ConsoleCommandProcessors = new Dictionary<string, Action<string[]>>() {
				{"/say", SendMessage},
				{"/me", SendAction},
				{"/join", JoinChannels},
				{"/j", JoinChannels},
				{"/part", PartChannels},
				{"/p", PartChannels},
				{"/list", ListChannels},
				{"/ls", ListChannels},
				{"/l", ListChannels},
				{"/listchannels", ListChannels},
				{"/sayall", SendMessageToAll},
				{"/reload", ReloadConfig},
				{"/help", PrintHelp},
				{"/nick", ChangeName}
			};
		}

		public static void ReadCommands() {
			while (true) {
				string message = System.Console.ReadLine();
				string command;
				string[] args;
				string[] msg = message.TrimEnd(' ').Split(' ');

				command = msg[0];
				args = msg.Skip(1).ToArray();

				if (ConsoleCommandProcessors.ContainsKey(command)) {
                    try {
                        ConsoleCommandProcessors[command](args);
                    } catch (Exception ex) {
                        WriteConsoleMessage(string.Format("Unable to perform command: {0}", ex.Message), MessageDirection.Error);
                    }
                } else {
					irc.WriteLine(message);
				}
				
				Thread.Sleep(0);
			}
		}

		private static void OnChannelMessageReceived(object sender, IrcEventArgs e) {
			string messageSender = e.Data.Nick;
			string message = e.Data.Message;
			string command;
			string[] args;
			string[] msg;

			if (chatBots.ContainsKey(messageSender.ToLower())) {
				Regex regex = chatBots[messageSender.ToLower()];
				if (regex == null) { return; } // Ignore chatbots whose regex didn't load right
				var matches = regex.Match(message);
				if (!matches.Success) { return; } // Skip if there's no match.
				messageSender = matches.Groups["sender"].Value;
				message = matches.Groups["message"].Value;
			}

			msg = message.TrimEnd(' ').Split(' ');
			command = msg[0];
			args = msg.Skip(1).ToArray();

			if (ReadChatCommand(messageSender, e, command, args)) {
				return;
			}

			if (random.Next(randomChance) == 0) {
				int choice = random.Next(randomResponses.Count);
				string[] responses = randomResponses[choice].Split('\\');
				foreach (string response in responses) {
					if (response.StartsWith("/me ")) {
						SendAction(e.Data.Channel, response.Substring(4));
					} else {
						SendMessage(e.Data.Channel, response);
					}
					Thread.Sleep(lineTimeout);
				}
			}

			if (Regex.IsMatch(message, PlusPlusRegEx)) {
				LogMessage(e.Data.Channel, MessageDirection.In, "<{0}> {1}", e.Data.Nick, e.Data.Message);
				HandlePlusPlus(e.Data.Channel, messageSender, message);
			}
		}

		private static void HandlePlusPlus(string channel, string messageSender, string message) {
			foreach (var matchGroup in Regex.Matches(message, PlusPlusRegEx).Cast<Match>().GroupBy(m => m.Value)) {
				Match match = matchGroup.First();
				int change = matchGroup.Count();
				string name = match.Value.ToLowerInvariant();
				if (name == "") { continue; }

				using (Command = new MySqlCommand() { Connection = Connection })
				using (Connection.Open()) {
					// Get the final name
					name = getLinkedName(name) ?? name;
					// See if we're the same person
					messageSender = getLinkedName(name) ?? messageSender;
					// Punish cheaters by decrementing
					change *= messageSender.Equals(name, StringComparison.InvariantCultureIgnoreCase) ? -1 : 1;
					int score;
					// Get the existing score
					getKarma(name, out score);
					score += change;
					// Update the score
					int rows = Command.ExecuteFullUpdate(KarmaSqlCommands.InsertScore, new { Name = "@fname", Value = name }, new { Name = "@score", Value = score });
					if (rows == 0) {
						SendMessage(channel, "Could not update score for {0}.", name);
						return;
					}
					// Track the score change
					rows = Command.ExecuteFullUpdate(KarmaSqlCommands.TrackScoreChange, new { Name = "@name", Value = name }, new { Name = "@change", Value = change });
					if (rows == 0) {
						SendMessage(channel, "Could not add tracking row for {0}.", name);
					}
				}
			}
		}

		private static void WriteErrorToConsole(string format, params object[] args) {
            WriteConsoleMessage(string.Format(format, args), MessageDirection.Error);
		}

		private static void SendMessage(string channel, string format, params object[] args) {
			irc.SendMessage(SendType.Message, channel, string.Format(format, args));
			LogMessage(channel, MessageDirection.Out, format, args);
		}

		private static void SendAction(string channel, string format, params object[] args) {
			irc.SendMessage(SendType.Action, channel, string.Format(format, args));
			LogMessage(channel, MessageDirection.Out, "/me " + format, args);
		}

        private static string getDirectionString(MessageDirection direction) {
            ConsoleColor color;
            return getDirectionString(direction, out color);
        }

        private static string getDirectionString(MessageDirection direction, out ConsoleColor color) {
            string dirString = "";
            switch (direction) {
                case MessageDirection.In:
                    color = ConsoleColor.Green;
                    dirString = ">>>";
                    break;
                case MessageDirection.Out:
                    color = ConsoleColor.Cyan;
                    dirString = "<<<";
                    break;
                default:
                case MessageDirection.Notification:
                    color = ConsoleColor.Yellow;
                    dirString = "---";
                    break;
                case MessageDirection.Error:
                    color = ConsoleColor.Red;
                    dirString = "!!!";
                    break;
            }
            return dirString;
        }

        private static void LogMessage(string channel, MessageDirection direction, string format, params object[] args) {
            string dir = getDirectionString(direction);

            try {
				using (StreamWriter sw = new StreamWriter(logLocation, true)) {
					sw.WriteLine("{0} [{1}] {2} {3}", dir,
						DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss.fff"),
						channel, string.Format(format, args)
					);
				}
			} catch { }
		}

		private static bool ReadChatCommand(string messageSender, IrcEventArgs e, string command, string[] args) {
			if (ChatCommandProcessors.ContainsKey(command)) {
				LogMessage(e.Data.Channel, MessageDirection.In, "<{0}> {1}", e.Data.Nick, e.Data.Message);
                try {
                    ChatCommandProcessors[command](messageSender, e.Data.Channel, command, args);
                } catch (Exception ex) {
                    SendMessage(e.Data.Channel, "{1}: Unable to perform \"{2}\": {0}", ex.Message, e.Data.Nick, command);
                    LogMessage(e.Data.Channel, MessageDirection.Error, "ERROR: ^^^^^^^^^^^^^^^^^^^^");
                }
				return true;
			}
			return false;
		}

		private static void ResetScope() {
			random = new Random();
		}

		private static bool HasPrivs(string userName, Channel channel) {
			foreach (ChannelUser user in channel.Users.Values) {
				if (user.Nick == userName) {
					if (chatBots.ContainsKey(user.Nick)) {
						return false;
					}
					return (user.IsOp || user.IsVoice);
				}
			}

			return false;
		}

		private static bool HasPrivs(string userName, string channelName) {
			return HasPrivs(userName, irc.GetChannel(channelName));
		}

		private static void OnRawMessage(object sender, IrcEventArgs e) {
			WriteConsoleMessage(e.Data.RawMessage, MessageDirection.In);
		}

		private static void OnWriteLine(object sender, WriteLineEventArgs e) {
			WriteConsoleMessage(e.Line, MessageDirection.Out);
		}

		private static void WriteConsoleMessage(string message, MessageDirection direction) {
			ConsoleColor color = ConsoleColor.White;
            string dirString = getDirectionString(direction, out color);

            Console.ForegroundColor = color;
			Console.WriteLine("[{0}] {1} {2}", DateTime.Now.ToString("HH:mm:ss"), dirString, message);
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}
